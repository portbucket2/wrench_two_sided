﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class senseLevelState : MonoBehaviour
{

    public int levelValue;
    public GameObject levelNum;
    public GameObject lockedIcon;
    public GameObject gameController;
    // Start is called before the first frame update
    void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController");
        foreach (Transform child in this.transform)
        {
            if(child.tag == "lockedImage")
            {
                lockedIcon = child.gameObject;
            }
            else if(child.tag == "levelNum")
            {
                levelNum = child.gameObject;
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        Unlocking();
        
    }

    void Unlocking()
    {
        if(gameController.GetComponent<gameController>().levelUnlocked >= levelValue)
        {
            lockedIcon.SetActive(false);
            levelNum.SetActive(true);
        }
        else
        {
            lockedIcon.SetActive(true);
            levelNum.SetActive(false);
        }
    }
}
