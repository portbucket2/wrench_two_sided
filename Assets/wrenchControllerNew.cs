﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wrenchControllerNew : MonoBehaviour {

    public float rotSpeed;
    public Transform pivot_one;
    public Transform pivot_two;

    public float snapAngle;
    public bool firstQuadrant;
    public bool secondQuadrant;
    public bool thirdQuadrant;
    public bool fourthQuadrant;
    public Vector3 rotAngles;
    public float rotAnglesZ;
    public float angleZ;
    public bool snapIt;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        rotateWrench();
        SnapAtRightAngles();
        pushToSnapAngle();

    }
    void rotateWrench()
    {

        angleZ += rotSpeed;
        this.transform.rotation = Quaternion.Euler(0, 0, angleZ);
        
        rotAngles = Quaternion.ToEulerAngles(this.transform.rotation);
        rotAnglesZ = Mathf.Rad2Deg * (rotAngles.z);
        if(rotAnglesZ < 0)
        {
            rotAnglesZ += 360;
        }
        if (angleZ > 360)
        {
            angleZ -= 360;
        }
        //wrenchMover.transform.Rotate(0, 0, rotSpeed+upFac+downFac );
        this.transform.Rotate(0, 0, rotSpeed);
        //float rotZ = rotAngles.z * 57.3f;
        if ((rotAngles.z * 57.3f) >0 && (rotAngles.z * 57.3f < 90) )
        {
            firstQuadrant  =  true;
            secondQuadrant =  false;
            thirdQuadrant  =  false;
            fourthQuadrant =  false;
        }
        else if ((rotAngles.z * 57.3f) > 90 && (rotAngles.z * 57.3f < 180))
        {
            firstQuadrant = false;
            secondQuadrant = true;
            thirdQuadrant = false;
            fourthQuadrant = false;
        }
        else if ((rotAngles.z * 57.3f) > -180 && (rotAngles.z * 57.3f < -90))
        {
            firstQuadrant = false;
            secondQuadrant = false;
            thirdQuadrant = true;
            fourthQuadrant = false;
        }
        else if ((rotAngles.z * 57.3f) > -90 && (rotAngles.z * 57.3f < 0))
        {
            firstQuadrant = false;
            secondQuadrant = false;
            thirdQuadrant = false;
            fourthQuadrant = true;
        }
    }

    void SnapAtRightAngles()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if (firstQuadrant == true)
            {
                snapAngle = 90;
                //this.transform.rotation = Quaternion.Euler(0, 0, snapAngle);
            }
            else if (secondQuadrant == true)
            {
                snapAngle = 180;
                //this.transform.rotation = Quaternion.Euler(0, 0, snapAngle);
            }
            else if (thirdQuadrant == true)
            {
                snapAngle = 270;
                //this.transform.rotation = Quaternion.Euler(0, 0, snapAngle);
            }
            else if (fourthQuadrant == true)
            {
                snapAngle = 360;
                //this.transform.rotation = Quaternion.Euler(0, 0, snapAngle);
            }
            snapIt = true;
        }
        
    }
    void pushToSnapAngle()
    {
        if (snapAngle - rotAnglesZ < 80 && snapAngle - rotAnglesZ > 0 && snapIt == true)
        {
            //this.transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(rotAngles.z * 57.3f, snapAngle, Time.deltaTime * 10));
            angleZ = Mathf.Lerp(angleZ, snapAngle, Time.deltaTime * 20);

        }
        if (Mathf.Round(snapAngle - rotAnglesZ) < 10 && snapIt == true)
        {
            angleZ = snapAngle;
            snapIt = false;
        }


    }
}
