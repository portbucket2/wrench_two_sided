﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collectedBalls : MonoBehaviour {

    public int ballsCollected;
    //public int level;
    public List<GameObject> balls;
    public bool gotBallOne;
    public bool gotBallTwo;
    public bool gotBallThree;

    // Use this for initialization
    void Start () {
        //level = PlayerPrefs.GetInt("level", level);
        gotBallOne = gotBallTwo = gotBallThree = false;

    }
	
	// Update is called once per frame
	void Update () {
        ballsCollected = balls.Count;
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "ball1" && gotBallOne == false)
        {
            balls.Add(other.gameObject);
            gotBallOne = true;
        }
        if (other.tag == "ball2" && gotBallTwo == false)
        {
            balls.Add(other.gameObject);
            gotBallTwo = true;
        }
        if (other.tag == "ball3" && gotBallThree == false)
        {
            balls.Add(other.gameObject);
            gotBallThree = true;
        }
    }
}
