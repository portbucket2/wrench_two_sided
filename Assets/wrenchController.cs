﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wrenchController : MonoBehaviour {
    public float rotSpeed;
    public float upFac;
    public float downFac;
    public GameObject wrenchHolder;
    public GameObject wrenchMover;
    public Transform pivot_one;
    public Transform pivot_two;
    public bool isflipped;
    public Transform pivot;
    public Vector3 pivotPos;
    public Transform otherPivot;
    public bool isCloseToScrew;

    //public Transform[] screws;
    public GameObject[] screws;
    //public GameObject[] screwss;
    public List<GameObject> screwList;
    //public Transform[] screwsTest;
    public Vector3 screwPos;
    public Vector3 hookScrewPos;
    public float dist;
    public float tempDist;
    public float snapRange;
    Vector3 desiredPos;

    public bool isScrewTimerReset;

    public bool isScrewUp;
    public bool isScrewDown;
    public bool isScrewRight;
    public bool isScrewLeft;
    


    // Use this for initialization
    void Start () {
        //screws = GameObject.FindGameObjectsWithTag("screws");
        screws = screwList.ToArray();
        pivot = pivot_one;
        otherPivot = pivot_two;
        isScrewTimerReset = false;

    }
	
	// Update is called once per frame
	void Update () {
        screws = screwList.ToArray();
        checkScrewPos();
        rotateWrench();
        //swingWrench();
        
        flipWrench();
        //checkScrewPos();
        pivotPos = pivot.transform.position;
        
        //this.transform.position = Vector3.Lerp(this.transform.position, desiredPos, Time.deltaTime * 30f);

    }

    void rotateWrench()
    {
        //wrenchMover.transform.Rotate(0, 0, rotSpeed+upFac+downFac );
        wrenchMover.transform.Rotate(0, 0, rotSpeed );
    }
    void swingWrench()
    {
        Vector3 rot = Quaternion.ToEulerAngles(wrenchMover.transform.localRotation);
        //upFac = Mathf.Lerp(5, 0, (rot.z*57.3f) / 180);
        //downFac = Mathf.Lerp(5,0, (rot.z * 57.3f) / -180);
        if(rot.z > 0)
        {
            downFac = Mathf.Lerp(downFac, 0, Time.deltaTime* 20f);
        }
        else
        {
            downFac = Mathf.Lerp(5, 0, (rot.z * 57.3f) / -180);
        }
        if (rot.z < 0)
        {
            upFac = Mathf.Lerp(upFac, 0, Time.deltaTime * 20f);
        }
        else
        {
            upFac = Mathf.Lerp(5, 0, (rot.z * 57.3f) / 180);
        }



    }

    void flipWrench()
    {
        //if(isflipped == false)
        //{
        //    pivot = pivot_one;
        //    otherPivot = pivot_two;
        //    //wrenchHolder.transform.localPosition = new Vector3(0, -2, 0);
        //}
        //else
        //{
        //    pivot = pivot_two;
        //    otherPivot = pivot_one;
        //    //wrenchHolder.transform.localPosition = new Vector3(0, 2, 0);
        //}
        //dist = Vector3.Distance(hookScrewPos, otherPivot.transform.position);

        if (Input.GetMouseButtonDown(0))
        {
            checkScrewPos();
            if (isflipped == false)
            {
                //dist = Vector3.Distance(screwPos, pivot_two.transform.position);
                if(dist< snapRange)
                {
                    
                    this.transform.position = hookScrewPos;
                    wrenchHolder.transform.localPosition = new Vector3(0, 2, 0);
                    isScrewTimerReset = true;
                    Invoke("ResetScrewTimer", 0.5f);
                    //desiredPos = hookScrewPos;
                    //this.transform.position = Vector3.Lerp(this.transform.position, hookScrewPos, Time.deltaTime * 10f);
                }
                //else
                //{
                //    this.transform.position = pivot_two.transform.position;
                //    //desiredPos = pivot_two.transform.position;
                //    this.GetComponent<Rigidbody>().useGravity = true;
                //}
                //this.transform.position = pivot_two.transform.position;
                //wrenchHolder.transform.localPosition = new Vector3(0, 2, 0);
            }
            else
            {
                //dist = Vector3.Distance(screwPos, pivot_one.transform.position);
                if (dist < snapRange)
                {
                    //this.transform.position = screwPos;
                    this.transform.position = hookScrewPos;
                    wrenchHolder.transform.localPosition = new Vector3(0, -2, 0);
                    isScrewTimerReset = true;
                    Invoke("ResetScrewTimer", 0.5f);
                    //desiredPos = hookScrewPos;
                    //this.transform.position = Vector3.Lerp(this.transform.position, hookScrewPos, Time.deltaTime * 10f);
                }
                //else
                //{
                //    this.transform.position = pivot_one.transform.position;
                //    //desiredPos = pivot_one.transform.position;
                //    this.GetComponent<Rigidbody>().useGravity = true;
                //    //this.GetComponent<Rigidbody>().AddForce(this.transform.position.x * 300, 0, -500);
                //}
                //this.transform.position = pivot_one.transform.position;
                //wrenchHolder.transform.localPosition = new Vector3(0, -2, 0);
            }

            if (isflipped == false)
            {
                isflipped = true;
            }
            else
            {
                isflipped = false;
            }
        }
        if (isflipped == false)
        {
            pivot = pivot_one;
            otherPivot = pivot_two;
            //wrenchHolder.transform.localPosition = new Vector3(0, -2, 0);
        }
        else
        {
            pivot = pivot_two;
            otherPivot = pivot_one;
            //wrenchHolder.transform.localPosition = new Vector3(0, 2, 0);
        }
        dist = Vector3.Distance(hookScrewPos, otherPivot.transform.position);
    }
    void checkScrewPos()
    {

        
        

        for (int i = 0; i < screws.Length; i++)
        {
            screwPos = screws[i].transform.position;
            tempDist = Vector3.Distance(screwPos, otherPivot.transform.position);
            if(tempDist < snapRange)
            {
                dist = tempDist;
                hookScrewPos = screwPos;
            }
            //if (isflipped == false)
            //{
            //    dist = Vector3.Distance(screwPos, pivot_two.transform.position);
            //    if(dist< 0.5f)
            //    {
            //        isCloseToScrew = true;
            //    }
            //    else
            //    {
            //        isCloseToScrew = false ;
            //    }
            //    //this.transform.position = pivot_two.transform.position;
            //    //wrenchHolder.transform.localPosition = new Vector3(0, 2, 0);
            //}
            //else
            //{
            //    dist = Vector3.Distance(screwPos, pivot_one.transform.position);
            //    if (dist < 0.5f)
            //    {
            //        isCloseToScrew = true;
            //    }
            //    else
            //    {
            //        isCloseToScrew = false;
            //    }
            //    //this.transform.position = pivot_one.transform.position;
            //    //wrenchHolder.transform.localPosition = new Vector3(0, -2, 0);
            //}
        }
    }

    void ResetScrewTimer()
    {
        if(isScrewTimerReset == true)
        {
            isScrewTimerReset = false;
        }
    }
    void SensingScrews()
    {
                                 
    }
    void AssignScrews()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag== "screws")
        {
            
            screwList.Add(other.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "screws")
        {
            
            screwList.Remove(other.gameObject);
        }
    }
}
