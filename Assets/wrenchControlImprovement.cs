﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wrenchControlImprovement : MonoBehaviour {

    public GameObject pivotUp;
    public GameObject pivotDown;
    public GameObject wrenchHolder;

    public float rotSpeed;


    public bool upPivotMode;
    public bool downPivotMode;

    public bool jumpMode;

    public Vector3 pivotPos;

    Rigidbody rb;

    public bool isScrewTimerReset;

    // Use this for initialization
    void Start () {
        upPivotMode = true;
        rb = this.GetComponent<Rigidbody>();
		
	}
	
	// Update is called once per frame
	void Update () {
        rotateWrench();
        FlipWrenchMode();

    }

    void rotateWrench()
    {
        this.transform.Rotate(0, 0, rotSpeed);
    }

    void FlipWrenchMode()
    {
        if (Input.GetMouseButtonDown(0) && jumpMode == true)
        {
            if (upPivotMode)
            {
                upPivotMode = false;
                downPivotMode = true;
                //this.transform.position = pivotDown.transform.position;
                
                wrenchHolder.transform.localPosition = new Vector3(0, 4, 0);
            }
            else 
            {
                downPivotMode = false;
                upPivotMode = true;
                //this.transform.position = pivotUp.transform.position;
                wrenchHolder.transform.localPosition = new Vector3(0, 0, 0);
            }
            this.transform.position = pivotPos;
            jumpMode = false;
            isScrewTimerReset = true;
            Invoke("ResetScrewTimer", 0.5f);
            //if (jumpMode == true)
            //{
            //    this.transform.position = targetPos;
            //}
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "screws")
        {
            //Debug.Log("screwIt");
            if(other.transform.position != this.transform.position )
            {
                //Debug.Log("screwIt");
                pivotPos = other.transform.position;
                jumpMode = true;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "screws")
        {
            //Debug.Log("screwIt");
            if (other.transform.position != this.transform.position)
            {
                //Debug.Log("screwIt");
                //targetPos = other.transform.position;
                jumpMode = false;
            }
        }
    }

    void ResetScrewTimer()
    {
        if (isScrewTimerReset == true)
        {
            isScrewTimerReset = false;
        }
    }
}
